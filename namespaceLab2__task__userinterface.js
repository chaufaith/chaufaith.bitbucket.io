var namespaceLab2__task__userinterface =
[
    [ "Task_User", "classLab2__task__userinterface_1_1Task__User.html", "classLab2__task__userinterface_1_1Task__User" ],
    [ "S0_init", "namespaceLab2__task__userinterface.html#a08ded6cd957c273bf744e9588e98ecf1", null ],
    [ "S1_wait_for_char", "namespaceLab2__task__userinterface.html#a866103543872f02da69694ba05d721c3", null ],
    [ "S2_collect_data", "namespaceLab2__task__userinterface.html#aa2d23370f3997edf7a87c957c8d7b1ed", null ],
    [ "S3_print_data", "namespaceLab2__task__userinterface.html#a6ed0f2ff5e6bd2718852fa105890c297", null ]
];