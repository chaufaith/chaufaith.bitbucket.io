var namespaces_dup =
[
    [ "BNO055", "namespaceBNO055.html", "namespaceBNO055" ],
    [ "DRV8847", "namespaceDRV8847.html", "namespaceDRV8847" ],
    [ "Lab0x01", "namespaceLab0x01.html", [
      [ "onButtonPressFCN", "namespaceLab0x01.html#a30a5cf8034929397cada3568b302d96a", null ],
      [ "reset_timer", "namespaceLab0x01.html#ac60cf12ea29eb4c53b0a55d65db78cf0", null ],
      [ "Update_SQW", "namespaceLab0x01.html#a42f381fbc03202e46c9f6341302e1cc8", null ],
      [ "Update_STW", "namespaceLab0x01.html#a9c249cdf2bf5c6987a6fbc9bfa0edab6", null ],
      [ "Update_SW", "namespaceLab0x01.html#ad3ba002383b13ab96adee5065115d986", null ],
      [ "update_timer", "namespaceLab0x01.html#a2411427ca814bad0117683ddc2488c06", null ],
      [ "ButtonInt", "namespaceLab0x01.html#a7b9c33e3119acd6ca7b401a1fb51bc9f", null ],
      [ "Current_Time", "namespaceLab0x01.html#abc29d8f75fd77ef432c8a6ec89c78f48", null ],
      [ "pinA5", "namespaceLab0x01.html#a85ea292058f905478f79d7b582f8cac8", null ],
      [ "pinC13", "namespaceLab0x01.html#ab7350683e4e73629aaa40f90ce9b45d9", null ],
      [ "press", "namespaceLab0x01.html#aac3971d5347e1872d825829b8a4cd4df", null ],
      [ "runs", "namespaceLab0x01.html#a0cd01b949f895ba36e5a15d76c57071a", null ],
      [ "startTime", "namespaceLab0x01.html#a77851af891cc0a0703ce1aeea2be0103", null ],
      [ "state", "namespaceLab0x01.html#af49dfbadfaec711466de0f806232bf66", null ],
      [ "stopTime", "namespaceLab0x01.html#a8016a3b8ce6e2b926c882c212d67c81f", null ],
      [ "t2ch1", "namespaceLab0x01.html#a3b58c14328a9821ec59cfb491f02bdf9", null ],
      [ "tim2", "namespaceLab0x01.html#a778761870b1d6720e614c945eaa72b9f", null ]
    ] ],
    [ "Lab2_main", "namespaceLab2__main.html", [
      [ "main", "namespaceLab2__main.html#a06d675eb55b831dd7bb4594ae66efea4", null ],
      [ "encoder1", "namespaceLab2__main.html#a01f2dffac75d089aa308dab09833e94f", null ],
      [ "pinB6", "namespaceLab2__main.html#a0f7eba73fd454d025ce0151a12dc8fd0", null ],
      [ "pinB7", "namespaceLab2__main.html#adb2738c3f1dd7c8bd3c362a288143897", null ]
    ] ],
    [ "Lab2_task_encoder", "namespaceLab2__task__encoder.html", "namespaceLab2__task__encoder" ],
    [ "Lab2_task_userinterface", "namespaceLab2__task__userinterface.html", "namespaceLab2__task__userinterface" ],
    [ "Lab3_main", "namespaceLab3__main.html", [
      [ "main", "namespaceLab3__main.html#a3f0a089ed5f774f8989ae83275f0bc56", null ]
    ] ],
    [ "Lab3_task_encoder", "namespaceLab3__task__encoder.html", "namespaceLab3__task__encoder" ],
    [ "Lab3_task_motor", "namespaceLab3__task__motor.html", "namespaceLab3__task__motor" ],
    [ "Lab3_task_userinterface", "namespaceLab3__task__userinterface.html", "namespaceLab3__task__userinterface" ],
    [ "Lab4_main", "namespaceLab4__main.html", [
      [ "main", "namespaceLab4__main.html#ab7fbca9ed43c6a5f7c04af0d2b96ca51", null ]
    ] ],
    [ "Lab4_task_encoder", "namespaceLab4__task__encoder.html", "namespaceLab4__task__encoder" ],
    [ "Lab4_task_motor", "namespaceLab4__task__motor.html", "namespaceLab4__task__motor" ],
    [ "Lab4_task_userinterface", "namespaceLab4__task__userinterface.html", "namespaceLab4__task__userinterface" ],
    [ "Lab5_BNO055", "namespaceLab5__BNO055.html", "namespaceLab5__BNO055" ],
    [ "LabFF_closedloop", "namespaceLabFF__closedloop.html", "namespaceLabFF__closedloop" ],
    [ "LabFF_main", "namespaceLabFF__main.html", [
      [ "main", "namespaceLabFF__main.html#a923c1f439d26af4d1da3cdb644da7679", null ]
    ] ],
    [ "LabFF_task_IMU", "namespaceLabFF__task__IMU.html", "namespaceLabFF__task__IMU" ],
    [ "LabFF_task_motor", "namespaceLabFF__task__motor.html", "namespaceLabFF__task__motor" ],
    [ "LabFF_task_panel", "namespaceLabFF__task__panel.html", "namespaceLabFF__task__panel" ],
    [ "LabFF_task_userinterface", "namespaceLabFF__task__userinterface.html", "namespaceLabFF__task__userinterface" ],
    [ "LabFF_touch_pan", "namespaceLabFF__touch__pan.html", "namespaceLabFF__touch__pan" ],
    [ "mainpage", "namespacemainpage.html", null ],
    [ "shares", "namespaceshares.html", "namespaceshares" ]
];