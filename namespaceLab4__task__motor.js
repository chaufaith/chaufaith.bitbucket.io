var namespaceLab4__task__motor =
[
    [ "Task_Motor", "classLab4__task__motor_1_1Task__Motor.html", "classLab4__task__motor_1_1Task__Motor" ],
    [ "S1_RUN", "namespaceLab4__task__motor.html#abc415d01c7d268917cfe264f9fb7596a", null ],
    [ "S2_STOP", "namespaceLab4__task__motor.html#a71f83e0b77612e19acf4f8146efe92ad", null ],
    [ "S3_FAULT", "namespaceLab4__task__motor.html#a65ebf2edb9f3931322b706918b39eaa8", null ],
    [ "S4_CLEAR_FAULT", "namespaceLab4__task__motor.html#aa7bf863110e853bb47bf35957b9ca9e2", null ],
    [ "S5_STEP_RESPONSE", "namespaceLab4__task__motor.html#ad0157feb396f2463bf8bf7f259a6f50e", null ]
];