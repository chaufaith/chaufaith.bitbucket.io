var classLab5__BNO055_1_1BNO055 =
[
    [ "__init__", "classLab5__BNO055_1_1BNO055.html#a9e1cb1703e36dc1bc46e68737b08c377", null ],
    [ "angular_vel", "classLab5__BNO055_1_1BNO055.html#aef1f96594fd00471ca76aa7bc39a95d6", null ],
    [ "euler_angle", "classLab5__BNO055_1_1BNO055.html#ae4e5fb93a3f3d562e60bc54d6c58751b", null ],
    [ "get_calib_coef", "classLab5__BNO055_1_1BNO055.html#af408972b55764d36c0bd9245c51183b5", null ],
    [ "get_calib_status", "classLab5__BNO055_1_1BNO055.html#adc9f69d7c216283cd64862397624b34d", null ],
    [ "set_calib_coef", "classLab5__BNO055_1_1BNO055.html#ae05f01203e296018257ecc4248011699", null ],
    [ "set_operating", "classLab5__BNO055_1_1BNO055.html#a41e0b64169807c564787d8b647a711eb", null ],
    [ "calib_coef", "classLab5__BNO055_1_1BNO055.html#a3a64eddc66357dbb0b511bcc28561668", null ],
    [ "i2c", "classLab5__BNO055_1_1BNO055.html#a5a2955ce4b80cfa797a76b14ebd7d76f", null ]
];