var classclosedloop_1_1ClosedLoop =
[
    [ "__init__", "classclosedloop_1_1ClosedLoop.html#ad1b9ee99ee60321850c33667c2fb0adc", null ],
    [ "get__Kp", "classclosedloop_1_1ClosedLoop.html#a6e4e75e69cd258a239a547f2db3d0642", null ],
    [ "run", "classclosedloop_1_1ClosedLoop.html#a8a5e26066b93ead620f3879f0aa38638", null ],
    [ "set__Kp", "classclosedloop_1_1ClosedLoop.html#a0ecc0733554e518a81bf2afe04606c63", null ],
    [ "error", "classclosedloop_1_1ClosedLoop.html#a5ef95e7ca63a2141f54ec785141d1432", null ],
    [ "gain", "classclosedloop_1_1ClosedLoop.html#ab45d5bcd1157c30cc881161922781d4d", null ],
    [ "Kp", "classclosedloop_1_1ClosedLoop.html#aae8c3c1b5cb36912bca6b14089e108f3", null ],
    [ "L", "classclosedloop_1_1ClosedLoop.html#a1e5405d3f994099318277aaabe71aeb4", null ],
    [ "measured", "classclosedloop_1_1ClosedLoop.html#a213ece790b42977765512f3324358fc0", null ],
    [ "reference", "classclosedloop_1_1ClosedLoop.html#ae05a5dd808235ae0c12a3dded340c754", null ],
    [ "sat_max", "classclosedloop_1_1ClosedLoop.html#aa3332ec9433fb16b0d38adb01b2573e2", null ],
    [ "sat_min", "classclosedloop_1_1ClosedLoop.html#a1b583e3f7f0b898fe4996be300f0166d", null ]
];