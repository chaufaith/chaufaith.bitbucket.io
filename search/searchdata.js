var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxyz",
  1: "bcdmqst",
  2: "bdlms",
  3: "bdlms",
  4: "_acdefgmnoprstuw",
  5: "bcdefgiklmnoprstuvxyz",
  6: "fhl"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

