var searchData=
[
  ['gain_0',['gain',['../classLab4__task__motor_1_1Task__Motor.html#adc19b788765d9e1774bb1ffaa89d2ea5',1,'Lab4_task_motor.Task_Motor.gain()'],['../classLabFF__closedloop_1_1ClosedLoop.html#af1c86727cbe961b4c48d2b07b98ebc24',1,'LabFF_closedloop.ClosedLoop.gain()']]],
  ['gain_5f1_1',['gain_1',['../classLab4__task__userinterface_1_1Task__User.html#a3def2cedf4294260bb7f958eac490c29',1,'Lab4_task_userinterface::Task_User']]],
  ['gain_5f2_2',['gain_2',['../classLab4__task__userinterface_1_1Task__User.html#a61c98f61668b7668fa7add0c10b68d41',1,'Lab4_task_userinterface::Task_User']]],
  ['get_3',['get',['../classshares_1_1Queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares::Queue']]],
  ['get_5f_5fkp_4',['get__Kp',['../classLabFF__closedloop_1_1ClosedLoop.html#a50683d8b6415697f51019ff77143c871',1,'LabFF_closedloop::ClosedLoop']]],
  ['get_5fcalib_5fcoef_5',['get_calib_coef',['../classBNO055_1_1BNO055.html#a0e97df5e8023d7d4f13c3137fadc5c9e',1,'BNO055.BNO055.get_calib_coef()'],['../classLab5__BNO055_1_1BNO055.html#af408972b55764d36c0bd9245c51183b5',1,'Lab5_BNO055.BNO055.get_calib_coef()']]],
  ['get_5fcalib_5fstatus_6',['get_calib_status',['../classBNO055_1_1BNO055.html#aaaf88e5ab026a8e07d72442fd372ac3a',1,'BNO055.BNO055.get_calib_status()'],['../classLab5__BNO055_1_1BNO055.html#adc9f69d7c216283cd64862397624b34d',1,'Lab5_BNO055.BNO055.get_calib_status()']]],
  ['get_5fcoords_7',['get_coords',['../classLabFF__touch__pan_1_1Touch__Pan.html#a8edafc3d7a0aac4fe92b254b4f60ae2a',1,'LabFF_touch_pan::Touch_Pan']]],
  ['get_5fx_8',['get_x',['../classLabFF__touch__pan_1_1Touch__Pan.html#a5ad1a3159ddb8e9a62a050f7a01073a8',1,'LabFF_touch_pan::Touch_Pan']]],
  ['get_5fy_9',['get_y',['../classLabFF__touch__pan_1_1Touch__Pan.html#a4648a869c86e5bbbefdad79c3da98bf3',1,'LabFF_touch_pan::Touch_Pan']]],
  ['get_5fz_10',['get_z',['../classLabFF__touch__pan_1_1Touch__Pan.html#a468f2567bbf3d137cacef6424862d69d',1,'LabFF_touch_pan::Touch_Pan']]]
];
