var searchData=
[
  ['set_5f_5fkp_0',['set__Kp',['../classLabFF__closedloop_1_1ClosedLoop.html#a290642b45068f4f0ea0aa2562f0cec09',1,'LabFF_closedloop::ClosedLoop']]],
  ['set_5fcalib_5fcoef_1',['set_calib_coef',['../classBNO055_1_1BNO055.html#a7b2841d5bb7ca978eda5e3d93ac213d7',1,'BNO055.BNO055.set_calib_coef()'],['../classLab5__BNO055_1_1BNO055.html#ae05f01203e296018257ecc4248011699',1,'Lab5_BNO055.BNO055.set_calib_coef()']]],
  ['set_5fduty_2',['set_duty',['../classDRV8847_1_1Motor.html#ae2e6c0feeb46de3f93c35e7f25a79a8b',1,'DRV8847::Motor']]],
  ['set_5foperating_3',['set_operating',['../classBNO055_1_1BNO055.html#aa5428355d5e7e3fec5fc81240f297e33',1,'BNO055.BNO055.set_operating()'],['../classLab5__BNO055_1_1BNO055.html#a41e0b64169807c564787d8b647a711eb',1,'Lab5_BNO055.BNO055.set_operating()']]]
];
