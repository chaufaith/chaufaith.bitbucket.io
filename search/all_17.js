var searchData=
[
  ['x_5fadc_0',['x_ADC',['../classLabFF__touch__pan_1_1Touch__Pan.html#a404aee93be488e3a70a1558f3e2ef54f',1,'LabFF_touch_pan::Touch_Pan']]],
  ['x_5fadc_5fcal_1',['x_ADC_cal',['../classLabFF__touch__pan_1_1Touch__Pan.html#a49f756bcae24c2c78940e08aa24e4f6c',1,'LabFF_touch_pan::Touch_Pan']]],
  ['x_5farray_2',['x_array',['../namespaceLabFF__task__userinterface.html#a47e5dee5c3ade912c9307cf0b9125fd7',1,'LabFF_task_userinterface']]],
  ['x_5fm_3',['x_m',['../classLabFF__touch__pan_1_1Touch__Pan.html#a33a3487572881a705e86f099ad4422de',1,'LabFF_touch_pan::Touch_Pan']]],
  ['x_5fp_4',['x_p',['../classLabFF__touch__pan_1_1Touch__Pan.html#aae4e0c08ec62a8ce37a8e3ad892bdd67',1,'LabFF_touch_pan::Touch_Pan']]],
  ['x_5fvelocity_5',['x_velocity',['../classLabFF__task__panel_1_1Task__Panel.html#a421fbe53cd3e20654cf98a4a15aaf341',1,'LabFF_task_panel::Task_Panel']]],
  ['x_5fwidth_6',['x_width',['../classLabFF__touch__pan_1_1Touch__Pan.html#a557f27367645e678d104a33529b492b4',1,'LabFF_touch_pan::Touch_Pan']]],
  ['xd_5farray_7',['xd_array',['../namespaceLabFF__task__userinterface.html#a150baaf73005c61953fe7fa4df705194',1,'LabFF_task_userinterface']]]
];
