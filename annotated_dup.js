var annotated_dup =
[
    [ "BNO055", "namespaceBNO055.html", [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "DRV8847", "namespaceDRV8847.html", [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "Lab2_task_encoder", "namespaceLab2__task__encoder.html", [
      [ "Task_Encoder", "classLab2__task__encoder_1_1Task__Encoder.html", "classLab2__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab2_task_userinterface", "namespaceLab2__task__userinterface.html", [
      [ "Task_User", "classLab2__task__userinterface_1_1Task__User.html", "classLab2__task__userinterface_1_1Task__User" ]
    ] ],
    [ "Lab3_task_encoder", "namespaceLab3__task__encoder.html", [
      [ "Task_Encoder", "classLab3__task__encoder_1_1Task__Encoder.html", "classLab3__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab3_task_motor", "namespaceLab3__task__motor.html", [
      [ "Task_Motor", "classLab3__task__motor_1_1Task__Motor.html", "classLab3__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab3_task_userinterface", "namespaceLab3__task__userinterface.html", [
      [ "Task_User", "classLab3__task__userinterface_1_1Task__User.html", "classLab3__task__userinterface_1_1Task__User" ]
    ] ],
    [ "Lab4_task_encoder", "namespaceLab4__task__encoder.html", [
      [ "Task_Encoder", "classLab4__task__encoder_1_1Task__Encoder.html", "classLab4__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab4_task_motor", "namespaceLab4__task__motor.html", [
      [ "Task_Motor", "classLab4__task__motor_1_1Task__Motor.html", "classLab4__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab4_task_userinterface", "namespaceLab4__task__userinterface.html", [
      [ "Task_User", "classLab4__task__userinterface_1_1Task__User.html", "classLab4__task__userinterface_1_1Task__User" ]
    ] ],
    [ "Lab5_BNO055", "namespaceLab5__BNO055.html", [
      [ "BNO055", "classLab5__BNO055_1_1BNO055.html", "classLab5__BNO055_1_1BNO055" ]
    ] ],
    [ "LabFF_closedloop", "namespaceLabFF__closedloop.html", [
      [ "ClosedLoop", "classLabFF__closedloop_1_1ClosedLoop.html", "classLabFF__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "LabFF_task_IMU", "namespaceLabFF__task__IMU.html", [
      [ "Task_IMU", "classLabFF__task__IMU_1_1Task__IMU.html", "classLabFF__task__IMU_1_1Task__IMU" ]
    ] ],
    [ "LabFF_task_motor", "namespaceLabFF__task__motor.html", [
      [ "Task_Motor", "classLabFF__task__motor_1_1Task__Motor.html", "classLabFF__task__motor_1_1Task__Motor" ]
    ] ],
    [ "LabFF_task_panel", "namespaceLabFF__task__panel.html", [
      [ "Task_Panel", "classLabFF__task__panel_1_1Task__Panel.html", "classLabFF__task__panel_1_1Task__Panel" ]
    ] ],
    [ "LabFF_task_userinterface", "namespaceLabFF__task__userinterface.html", [
      [ "Task_User", "classLabFF__task__userinterface_1_1Task__User.html", "classLabFF__task__userinterface_1_1Task__User" ]
    ] ],
    [ "LabFF_touch_pan", "namespaceLabFF__touch__pan.html", [
      [ "Touch_Pan", "classLabFF__touch__pan_1_1Touch__Pan.html", "classLabFF__touch__pan_1_1Touch__Pan" ]
    ] ],
    [ "shares", "namespaceshares.html", [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ]
];