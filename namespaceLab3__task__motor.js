var namespaceLab3__task__motor =
[
    [ "Task_Motor", "classLab3__task__motor_1_1Task__Motor.html", "classLab3__task__motor_1_1Task__Motor" ],
    [ "dutycycle_1", "namespaceLab3__task__motor.html#af39c88aac11e219fddfaf13a07cb3af4", null ],
    [ "dutycycle_2", "namespaceLab3__task__motor.html#a99b787068a5ac86e21d10e2c8f9db9fb", null ],
    [ "S1_RUN", "namespaceLab3__task__motor.html#ac063b039dfd3f180e36d6e2d99e92f2d", null ],
    [ "S2_STOP", "namespaceLab3__task__motor.html#a5ee7704378362f795e90a6a5f655ac38", null ],
    [ "S3_FAULT", "namespaceLab3__task__motor.html#a8b4cf82ce8fc26bcb90e0fd411503164", null ],
    [ "S4_CLEAR_FAULT", "namespaceLab3__task__motor.html#a869ff67337d19f8baac0df7c20ba50f8", null ]
];